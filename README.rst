====================
Entry Points Example
====================
This is a companion repository for the article entitled,
`"Demystifying Setuptools Entry Points" <https://www.eflglobal.com/setuptools-entry-points/>`_.

It includes a few examples of how to use setuptools ``entry_points`` to install
custom command-line applications and define a pluggable interface for your
Python packages.

Packages
========

- ``command_line_app``: A simple example showing how to install a command-line
  application using setuptools entry points.
- ``fabricator_base``: A package that can be extended using entry points.
- ``fabricator_expansion``: A package that extends ``fabricator_base`` using
  entry points.
