# coding=utf-8
# Not importing ``unicode_literals`` because in Python 2 distutils,
# some values are expected to be byte strings.
from __future__ import absolute_import, division, print_function

from setuptools import setup

setup(
    name = 'fabricator-expansion',
    url = 'https://bitbucket.org/eflglobal/entry-points-example',
    version = '1.0.0',

    description =
        'This is a companion repository for the article entitled, '
        '"Demystifying Setuptools Entry Points" '
        '(https://www.eflglobal.com/setuptools-entry-points/).',

    entry_points = {
        'fabricator.machines': [
            'crusher=fabricator_expansion.machines:Crusher',
            'refiner=fabricator_expansion.machines:Refiner',
        ],
    },
)
