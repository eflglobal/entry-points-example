# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from fabricator_base.machines import Machine


class Crusher(Machine):
    """
    Crushes materials into dust.
    """
    def process(self, material):
        print('Crushed {material} into dust.'.format(material=material))
        return 'dust'


class Refiner(Machine):
    """
    Extracts contaminants from materials.
    """
    def process(self, material):
        print('Purified {material}.'.format(material=material))
        return 'slag'
