# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals


def say_hello():
    """
    Example entry point that will be installed as a console script.
    """
    print('Hello, world!')
