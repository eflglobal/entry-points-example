# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from abc import ABCMeta, abstractmethod

from six import with_metaclass


class Machine(with_metaclass(ABCMeta)):
    """
    A machine that can be installed in a Fabricator to extend its
    material processing capabilities.
    """
    @abstractmethod
    def process(self, material):
        raise NotImplementedError


class Furnace(Machine):
    """
    Smelts materials into ingots.
    """
    def process(self, material):
        print('Smelted {material} into ingots.'.format(material=material))
        return 'ingots'
