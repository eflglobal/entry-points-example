# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from six import moves

from fabricator_base.fabricator import Fabricator


def main():
    """
    Create a fabricator and enter a REPL, prompting the user for machines
    to activate.
    """
    starting_material = moves.input('Enter starting material: ')
    fabricator = Fabricator(starting_material)

    while True:
        machine_name =\
            moves.input('Enter machine to activate (blank input terminates): ')

        if machine_name:
            fabricator.process(machine_name)
        else:
            break

    print('Whirr! Click! Out comes {material}!'.format(
        material = fabricator.material,
    ))


if __name__ == '__main__':
    main()
