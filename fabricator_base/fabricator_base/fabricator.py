# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from pkg_resources import iter_entry_points


class Fabricator(object):
    """
    Simulates a science fiction fabricator.  It can turn raw materials into
    any finished product, so long as it has the proper machines installed.
    """
    def __init__(self, starting_material):
        super(Fabricator, self).__init__()

        self.material = starting_material

        self.machines = {}
        for entry_point in iter_entry_points('fabricator.machines'):
            self.machines[entry_point.name] = entry_point.load()

    def process(self, machine_name):
        """
        Processes the material using the specified machine.
        """
        try:
            selected_machine = self.machines[machine_name]
        except KeyError:
            print('Machine "{machine_name}" not installed!'.format(
                machine_name = machine_name,
            ))
        else:
            self.material = selected_machine().process(self.material)
