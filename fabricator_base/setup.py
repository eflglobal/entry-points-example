# coding=utf-8
# Not importing ``unicode_literals`` because in Python 2 distutils,
# some values are expected to be byte strings.
from __future__ import absolute_import, division, print_function

from setuptools import setup

setup(
    name = 'fabricator-base',
    url = 'https://bitbucket.org/eflglobal/entry-points-example',
    version = '1.0.0',

    description =
        'This is a companion repository for the article entitled, '
        '"Demystifying Setuptools Entry Points" '
        '(https://www.eflglobal.com/setuptools-entry-points/).',

    install_requires = ['six'],

    entry_points = {
        'console_scripts': [
            'fabricate=fabricator_base.controller:main',
        ],

        'fabricator.machines': [
            'furnace=fabricator_base.machines:Furnace',
        ],
    },
)
